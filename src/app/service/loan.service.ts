import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database'
import { AngularFireAuth } from 'angularfire2/auth';
import { map } from 'rxjs/operators';
import* as moment from 'moment'
@Injectable({
  providedIn: 'root'
})
export class LoanService {

  constructor(private db:AngularFireDatabase) { }

  getLoan(id){
    return this.db.list(`loan/${id}`).valueChanges()
  }
  setLoan (uid,author:string , bookName:string,loanDate:string,returnDate:string){
    
  const id = moment().unix()
  return this.db.object(`loan/${uid}/${id}`).set({
    author : author,
    bookname: bookName,
    loanDate: moment(loanDate).format('DD-MM-YYYY'),
    returnDate: moment(returnDate).format('DD-MM-YYYY')
    
  })
  }
 /* updateLoan(id,author:string,bookName:string,loanDate:string, returnDate:string){
    this.db.object(`user/${id}`).update({
      author : author,
      bookname: bookName,
      loanDate: loanDate,
      returnDate: returnDate 
    })
  }
  deleteLoan(id){
    this.db.object(`user/${id}`).remove()
  }*/

}


