import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database'
import { AngularFireAuth } from 'angularfire2/auth';
import { map } from 'rxjs/operators';
import* as moment from 'moment'

@Injectable({
  providedIn: 'root'
})
export class KonekDBService {

  constructor(private db: AngularFireDatabase, private authe: AngularFireAuth) { }

  getUser() {
    return this.db.list<any>('user').snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.val();
        const id = a.payload.key;
        return { id, ...data };
      });
    }));


  }


  setUser( name:string,email:string,registerDate:string,idCardNumber:string,phoneNumber :string) {
    const uid=moment().unix()
   return this.db.object(`user/${uid}`).set({
      name: name,
      email: email,
      registerDate: registerDate,
      idCardNumber: idCardNumber,
      phoneNumber : phoneNumber
    })
  }

  update(uid, name:string,email:string,idCardNumber:string,phoneNumber :string) {
    this.db.object(`user/${uid}`).update({
      name: name,
      email: email,
      idCardNumber: idCardNumber,
      phoneNumber : phoneNumber
    })
  }

  //delete
  deletea(uid) {
    this.db.object(`user/${uid}`).remove()
  }
}
