import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database'
import { AngularFireAuth } from 'angularfire2/auth';
import { map } from 'rxjs/operators';
import* as moment from 'moment'

@Injectable({
  providedIn: 'root'
})
export class BooklistService {

  constructor(private db :AngularFireDatabase) { }
  getBook() {
    return this.db.list<any>('book').snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.val();
        const id = a.payload.key;
        return { id, ...data };
      });
    }));


  }
  setBook (Value:string , viewValue:string,author:string){
    
    const id = moment().unix()
    return this.db.object(`book/${id}`).set({
      value : Value,
      author : author,
      viewvalue : viewValue
      
    })
    }
    updateBook(id,Value:string , viewValue:string,author:string ){
      debugger
      this.db.object(`book/${id}`).update({
        author : author,
        value: Value,
        viewvalue: viewValue 
      })
    }
}
