import { NgModule } from '@angular/core';

import { MatSnackBarModule, MatToolbarModule ,MatButtonModule,MatInputModule,MatTableModule,MatIconModule 
,MatCardModule,MatMenuModule,MatSelectModule,MatDatepickerModule,MatNativeDateModule} from '@angular/material'
const MaterialComponent =[MatSnackBarModule,
   MatToolbarModule,MatCardModule ,MatButtonModule,
   MatInputModule,MatTableModule,MatIconModule,MatNativeDateModule,MatSelectModule,MatMenuModule,MatDatepickerModule]

@NgModule({
  imports: [MaterialComponent],
  exports:[MaterialComponent]
})
export class MaterialModule { }
