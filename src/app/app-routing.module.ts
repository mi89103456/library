import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoanComponent } from './component/loan/loan.component';
import { PassingComponent } from './component/passing/passing.component';
import { BookComponent } from './component/book/book.component';


const routes: Routes = [
  {path:'user', component :PassingComponent},
  {path:'book', component :BookComponent},
  {path:'loan/:id', component :LoanComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
