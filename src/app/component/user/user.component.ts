import { Component, OnInit, Input } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import * as moment from 'moment'
import { KonekDBService } from '../../service/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import {user} from '../../models/property';
import { Router } from '@angular/router';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  @Input() public display;
  @Input() public get;
  user: user= new user();
  userid: string;
  liste: AngularFireList<any>;
  dataSource;
  
  //Read
  constructor(private koneksi: KonekDBService, private notif: MatSnackBar, private rout:Router) {
  
  }
  loanBook(uid){
    this.rout.navigate(['loan', uid])
  }
  setForm(element) {
    this.user.name = element.name;
    this.user.email = element.email;
    this.user.idCardNumber = element.idCardNumber;
    this.user.phoneNumber = element.phoneNumber;
  }

  create() {
    const registerDate= moment().toString()
    this.koneksi.setUser(this.user.name,this.user.email,registerDate,this.user.idCardNumber,this.user.phoneNumber).then(() => {
      this.showNotif('Berhasil', 'Success')
    }).catch((err: any) => {
      this.showNotif(err.message, 'blue-snackbar')
    })
  }

 update(id: string) {
    this.koneksi.update(id, this.user.name, this.user.email, this.user.idCardNumber, this.user.phoneNumber);
  }

  deletea(id:string) {
    this.koneksi.deletea(id);
  }

  showNotif(message: string, snackType: string) {
    this.notif.open(message, 'close', {
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: [snackType]
    })
  }

  openSnackBar(message: string, action: string, className: string) {
    this.notif.open(message, action, {
      duration: 2000,
      panelClass: [className]
    });
  }
  resetInput(){
    this.user.name = undefined
    this.user.email = undefined
    this.user.idCardNumber=undefined
    this.user.phoneNumber =undefined
  }

  ngOnInit() {

  }

}