import { Component, OnInit } from '@angular/core';
import { AngularFireList } from 'angularfire2/database';
import * as moment from 'moment'
import { LoanService } from '../../service/loan.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import {loan,book} from '../../models/property';
import { ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { BooklistService } from 'src/app/service/booklist.service';
@Component({
  selector: 'app-loan',
  templateUrl: './loan.component.html',
  styleUrls: ['./loan.component.css']
})
export class LoanComponent implements OnInit {
  book : book = new book();
  loan: loan = new loan();
  date = new FormControl(new Date())
  loanId:string
  listLoan:AngularFireList<any>;
  dataLoan: loan[]= []
  displayedColumns: string[] = ['no', 'author', 'bookname', 'loandate','returndate'];
  /*booknames : any[]=[
    {value : 'Solo Leveling' , viewValue : 'Solo Leveling' , author : 'Chu-Gong'},
    {value : 'Kimetsu No yaiba' , viewValue : 'Kimetsu No Yaiba', author: 'Koyoharu Gotoge'},
    {value : 'Naruto Shippudent' , viewValue : 'Naruto Shippudent', author: 'Masashi Kisimoto'}
  ]*/
  constructor(private con :LoanService, private notif:MatSnackBar , rout :ActivatedRoute, private cons :BooklistService) { 
    rout.params.subscribe(result =>{
      
      this.loan.id= result ['id']
    })
    this.con.getLoan(this.loan.id).subscribe((result : loan[])=>{
      
      this.dataLoan = result;
      console.log(result);
    })
    this.cons.getBook().subscribe
    ((result => {
      this.book.booknames = result;
      console.log(result);
    }))
  }
  eventSelection(event){
   
  this.loan.author = event.author
  this.loan.bookname = event.value  
  
   }
  maxInWeek(){
    const today = moment(this.loan.loanDate);
    const today2 = moment(this.loan.loanDate);
    const fromDate = today.startOf('week').format('DD-MM-YYYY');
    const toDate = today2.endOf('week').format('DD-MM-YYYY');
    const filter = this.dataLoan.filter(x => x.loanDate >= fromDate && x.loanDate <= toDate)
    debugger
    return filter.length;
  }
  maxInMonth(){
    const today = moment(this.loan.loanDate);
    const today2 = moment(this.loan.loanDate);
    const fromDatem = today.startOf('month').format('DD-MM-YYYY');
    const toDatem = today2.endOf('month').format('DD-MM-YYYY');
    const filter = this.dataLoan.filter(x => x.loanDate >= fromDatem && x.loanDate <= toDatem)
    debugger
    return filter.length;
  }
  addLoan(id){
  
    if(this.maxInMonth() <= 9){
       if(this.maxInWeek() <=2){
         debugger
        this.con.setLoan(id,this.loan.author, this.loan.bookname,this.loan.loanDate,this.loan.returnDate)
       }
       else{
         alert('User Ini Sudah meminjam 3 buku dalam minggu ini')
       }
    }
    else{
      alert('User Ini sudah meminjam 10 buku dalam bulan ini')
    }
    
  }
  ngOnInit() {
  }

}
