import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import {book} from '../../models/property';
import { ActivatedRoute } from '@angular/router';
import { BooklistService } from 'src/app/service/booklist.service';
@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  book: book = new book();
  bookId:string
  displayedColumns: string[] = ['no', 'value', 'viewvalue', 'author','mode'];
  constructor(private con :BooklistService, private notif:MatSnackBar) {
    this.con.getBook().subscribe
      ((result => {
        this.book.booknames = result;
        console.log(this.book.booknames);
      }))
   }
   addBook() {
    this.con.setBook(this.book.value, this.book.viewvalue, this.book.author).then(() => {
      this.showNotif('Berhasil', 'Success')
    }).catch((err: any) => {
      this.showNotif(err.message, 'blue-snackbar')
    })
  }
  updateBook(id) {
    debugger
    this.con.updateBook(id, this.book.value , this.book.viewvalue, this.book.author );
  }
  resetInput(){
    this.book.value = undefined
    this.book.viewvalue = undefined
    this.book.author=undefined
  }
  showNotif(message: string, snackType: string) {
    this.notif.open(message, 'close', {
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: [snackType]
    })
  }
  setForm(element){
    
    
    this.book.value = element.value
    this.book.viewvalue = element.viewvalue
    this.book.author = element.author

  }

  openSnackBar(message: string, action: string, className: string) {
    this.notif.open(message, action, {
      duration: 2000,
      panelClass: [className]
    });
  }

  ngOnInit() {
  }

}
