import { Component, OnInit } from '@angular/core';

import {KonekDBService} from '../../service/user.service'
import {user} from '../../models/property'
@Component({
  selector: 'app-passing',
  templateUrl: './passing.component.html',
  styleUrls: ['./passing.component.css']
})
export class PassingComponent implements OnInit {

  user : user = new user()
  displayedColumns: string[] = ['no', 'name', 'email', 'idcard','phonenumber','mode'];
  constructor(private koneksi:KonekDBService){
    this.koneksi.getUser().subscribe
      ((result => {
        this.user.users = result;
        console.log(this.user.users);
      }))

  }
 
  ngOnInit() {
  }

}
