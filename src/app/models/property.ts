export class user {
    
    users: any[];
    name:string;
    email:string;
    registerDate:string;
    idCardNumber:string;
    phoneNumber :string;
}

export class loan{
    loans:any[] = [];
    id : string;
    author:string;
    bookname:string;
    loanDate:string;
    returnDate:string;
}
export class book {
    booknames:any[]= [];
    id : string;
    value: string;
    author:string;
    viewvalue:string;
}