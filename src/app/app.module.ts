import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';

import { FormsModule } from '@angular/forms';
import { KonekDBService } from './service/user.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { NavComponent } from './component/nav/nav.component';
import { LoanComponent } from './component/loan/loan.component';
import { UserComponent } from './component/user/user.component';
import { PassingComponent } from './component/passing/passing.component';
import { BookComponent } from './component/book/book.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    LoanComponent,
    UserComponent,
    PassingComponent,
    BookComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    FormsModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [KonekDBService],
  bootstrap: [AppComponent]
})
export class AppModule { }
