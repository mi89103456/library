// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production : false,
  firebaseConfig : {
    apiKey: "AIzaSyCq_DsLNw7HTfjeok9PzS_Njv51415aXYQ",
    authDomain: "project11-3eb29.firebaseapp.com",
    databaseURL: "https://project11-3eb29.firebaseio.com",
    projectId: "project11-3eb29",
    storageBucket: "project11-3eb29.appspot.com",
    messagingSenderId: "123067489140",
    appId: "1:123067489140:web:4838199b514722071f2a21",
    measurementId: "G-X7B3T9410B"
  }
  };

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
